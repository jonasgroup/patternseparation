#include <stdio.h>
#include "hocdec.h"
#define IMPORT extern __declspec(dllimport)
IMPORT int nrnmpi_myid, nrn_nobanner_;

extern void _exp2synRand_reg();
extern void _gap_reg();
extern void _if3_reg();
extern void _if3Rand_reg();
extern void _if3Rand2_reg();
extern void _if3Rand3_reg();
extern void _if3Rand4_reg();
extern void _net_hh_w_reg();

void modl_reg(){
	//nrn_mswindll_stdio(stdin, stdout, stderr);
    if (!nrn_nobanner_) if (nrnmpi_myid < 1) {
	fprintf(stderr, "Additional mechanisms from files\n");

fprintf(stderr," exp2synRand.mod");
fprintf(stderr," gap.mod");
fprintf(stderr," if3.mod");
fprintf(stderr," if3Rand.mod");
fprintf(stderr," if3Rand2.mod");
fprintf(stderr," if3Rand3.mod");
fprintf(stderr," if3Rand4.mod");
fprintf(stderr," net_hh_w.mod");
fprintf(stderr, "\n");
    }
_exp2synRand_reg();
_gap_reg();
_if3_reg();
_if3Rand_reg();
_if3Rand2_reg();
_if3Rand3_reg();
_if3Rand4_reg();
_net_hh_w_reg();
}
