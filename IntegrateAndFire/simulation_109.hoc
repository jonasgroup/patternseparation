// Pattern separation network based on mixed integrate and fire / passive - active conductance based neurons 
// See Neuron book, p. 289 ff. 
// PJ, 01.02.2017
// Version 15 - Hybrid conductance and IF
// jIE jII positive - conversion into integrate and fire requires negative sign in the NetCon
// Version 22 - 02.01.2018, output files pattern by pattern 
// Version 24 - 04.01.2018, pConnect for sigma >= 100 replaced by equal distribution, connectivity for unidirectional and bidirectional connections specified separately 
// Taum in IntFire2 changed from 20 to 15 ms 
// Version 25 - 11.01.2018 - final revisions 
// Version 26 - 17.01.2018 - larger subset of cells in voltage output 
// Version 27 - 23.01.2018 - random efficacy for EI and IE synapses 
// Version 28, 29 - 29.01.2018 - motifs in connectivity matrix 
// Version 30 - put motifs on the background of random connectivity, gap junctions revised, autapses suppressed 
// Version 31 - motifs disabled again 
// Version 32, 33 - matrix reading from file incorporated

// Based on file : if33.hoc 

dt = 0.005  // 0.025 in WB model
steps_per_ms = 10 // for plotting 
celsius = 6.3 // to use the WB model properly 

strdef pathIn, pathOut, pathWD
strdef expression

pathIn = "../patterns"
pathOut = "sim109"
pathWD = ""
matrixReadFlag = 0  // flag to control reading connectivity matrix from file 
matrixWriteFlag = 0  // flag to control writing connectivity matrix to file 
nIStep = 10              // sample subset of neurons with step 
nEStep = 1000


chdir(pathWD)
sprint(expression, "mkdir %s", pathOut) 
print "Trying to generate new directory ", expression
system(expression)
objref strobj   // initialize string functions 
strobj = new StringFunctions()


// Variable time step - optional 
// cvode_active(1)   // cvode_active(1) activates variable time step 
// cvode.atol(0.001) // sets absolute tolerance for variable time step  
// cvode.use_local_dt(1) // use local dt for individual cells 
// cvode.current_method() // Should be: 1301, 1 for local, 3 for cvode, and 0 1 


nECells = 1000000 // 1000000    // numbers of E cells and I cells (10000 and 100 for test purposes)  
nICells = 5000 // 5000
driveScale = 2 

tstop = 50 // 50 - max simulation time in ms 
delMax = 50 // 10 - synaptic latency in ms 
// ---------------------------------------------------------------
cEI = 0.05 // 0.1 - peak connection probability for E to I synapses 
sigmaEI = 0.015 // 0.03 - standard deviation of connection probability for E to I synapses 
jEI = 0.004 // 0.008 - synaptic weight for E to I synapses in �S 
// ---------------------------------------------------------------
cIE = 1. // 0.3 - dto for I to E synapses 
sigmaIE = 0.03 // 0.06
jIE = 0.1 // 0.2
shift = 0 // 0 - shift of peak of inhibitory connection probability 
// ---------------------------------------------------------------
cIIChem = 0.6 // 0.3 - peak connection probability for I to I synapses 
sigmaIIChem = 0.015 // 0.06 - standard deviation of connection probability for I to I synapses
jIIChem = 0.01 // 0.02 - synaptic weight for I to I synapses
// ---------------------------------------------------------------
cIIGap = 0.6 // 0.6 - peak connection probability for gap junctions
gapResistance = 300 // 300 - gap junction transcellular resistance in MegaOhm 
sigmaIIGap = 0.015 // 0.03 - standard deviation of connection probability for gap junctions
// ---------------------------------------------------------------
rSeed = 10      // seed for random number generators
// ---------------------------------------------------------------
cellTy = 0      // Integrate and fire models for excitatory cells   
plotFlag = 0    //  0 = no GUI windows, 1 = GUI windows  
fileFlag = 1    // 0 = don't write output to file, 1 = write output to file 

objref f1, f2, f3, f4 // files 
// f1 = drive_iteration.in 
// f2 = pattern.out
// f3 = voltage.out (first subset of I cells, then subset of E cells)
// f4 = raster.out

// thresholdE = -50 // threshold in mV for E neurons 
thresholdI = 0 // threshold in mV - overshoot for I neurons 
// -------------------------------------------------------------------------------------
// step 1 - define templates (classes)

begintemplate ECell // ECells as integrate-and-fire models
  public infi
  objref infi
  proc init() {
    infi = new IntFire2()
    infi.taum = 15 // membrane time constant in ms 
    infi.taus = 30 // synaptic time constant in ms 
    infi.ib = 1  // constant current input 
  }
endtemplate ECell

// Note: taus > taum, otherwise error messages like this Intfire2 iter 11 x=0.900568 f=1 df=-0.0861853 dx=1.97865e-011 a=2.38732 b=436.443 c=-437.832 r=1.00333


begintemplate ICell2   // I cells as conductance based models
  public soma, ncE, ncI
  create soma
  objref ncE, ncI

  proc init() {
    soma { nseg = 1 Ra = 200 diam = 70 L = 70 } // to give reasonable input resistance and membrane time constant - Ra not needed 
	vRest = -65 // in mV 
    forall { 
	v = vRest // initialize resting potential 
    cm = 1.0  // in �F cm-2; gives 10 ms time constant 	
	insert hh_w   // insert active conductances 
	gl_hh_w = 0.0001 // im S cm-2
	el_hh_w = -65.7116 // reversal potential of leak conductance = resting potential
	}
	access soma
    ncE = new Exp2Syn(0.5) // exponential rise 
    ncE.tau1 = 0.1
	ncE.tau2 = 1  // fast excitation at BC-BC synapses 
    ncE.e = 0
	ncI = new Exp2Syn(0.5)
    ncI.tau1 = 0.1
    ncI.tau2 = 2.5 // fast inhibition at BC-BC synapses 
    ncI.e = -65 // shunting inhibition
  }
endtemplate ICell2

print "Step 1 finished"
// -------------------------------------------------------------------------------------
// step 2 - generate cells (objects) 

objref ECells, ICells 
ECells = new List()
ICells = new List()
for i = 0, nECells-1 ECells.append(new ECell())    // use integrate and fire 
for i = 0, nICells-1 ICells.append(new ICell2()) 

print "Step 2 finished"
// -------------------------------------------------------------------------------------
// step 3 - initialize random number generators  

objref rand                     // generate simple uniform random numbers between 0 and 1  
rand = new Random(rSeed)
rand.uniform(0, 1)               

cvWeight = 0
objref randJ                  // generate lognormally distributed drive values 
randJ = new Random(rSeed+2)
randJ.lognormal(1, cvWeight^2)   // mean and variance 
randJ.repick()

// use repick from now on, faster 

print "Step 3 finished"
// -------------------------------------------------------------------------------------
// step 4 - define functions and procedures   

func dist() { local myDist      // calculate distance between 2 neurons; par1 = first index, par2 = last index (normalized)
  myDist = abs($1 - $2)
  if(myDist < 0.5) return myDist else return abs(myDist-1)   // close to implement ring 
}

func pConnect() { local distance, sigma, value, mu  // normal distribution = Gaussian, peak normalized to 1 
  distance = $1
  sigma = $2
  mu = $3
  if (sigma < 100) value = exp(-(distance - mu)^2 / (2*sigma^2)) else value = 1
  return value 
}

slope = 0.01
func pConnect2() { local distance, mu, slope      // sigmoidal function = logistic, peak 1 
  distance = abs($1)
  mu = $2
//  slope = $3
  
  value = 1 / (1 + exp((distance - mu) / slope))   
  return value 
}


objref spikey, spikesE, spikesI
objref gRasterE, gRasterI

proc initRasterPlot2() {
  gRasterE = new Graph(0) // 0 = allow for size and place 
  gRasterE.view(0, 0, tstop, nECells, 700, 100, 300, 200) // last four pars: left, top, width, height
  gRasterI = new Graph(0)
  gRasterI.view(0, 0, tstop, nICells, 1300, 100, 300, 200) 
}

proc showRasterPlot2() { localobj spikes  // shows raster plot; par1 = spikesE or I, par2 = color (1 = black, 2 = red, 3 = blue, 4 = green, etc., par3 = shift
  gRasterE.erase_all()
  gRasterI.erase_all()
  for i = 0, spikesE.count()-1 {
    spikey = spikesE.object(i).c // create a vector that has as many elements as the number of spikes the cell fired
    spikey.fill(i+1) // fill those elements with an integer that is 1 larger than the ordinal position of that cell
    spikey.mark(gRasterE, spikesE.object(i), "O", 5, 1) // plot filled circles of size 5 and color 1 = black - vector class, not Graphics
  }
  for i = 0, spikesI.count()-1 {
    spikey = spikesI.object(i).c // create a vector that has as many elements as the number of spikes the cell fired
    spikey.fill(i+1) // fill those elements with an integer that is 1 larger than the ordinal position of that cell
    spikey.mark(gRasterI, spikesI.object(i), "O", 5, 2) // plot filled circles of size 5 and color 2 = red ...   - vector class, not Graphics
  }
} 

proc outputRasterPlot2() {
  f4 = new File()
  
  if (strobj.len(pathOut) > 0) sprint(expression, "%s/raster_%d.out", pathOut, iteration) else sprint(expression, "raster_%d.out", iteration)
  f4.wopen(expression)

  for i = 0, nICells-1 for j = 0, spikesI.object(i).size-1 f4.printf("ICells  %g %g \n", i+1, spikesI.object(i).x[j]) 
  for i = 0, nECells-1 for j = 0, spikesE.object(i).size-1 f4.printf("ECells %g %g \n", i+1, spikesE.object(i).x[j])
  f4.close()
}

objectvar windowE, windowI  // window plot for subset of E cells 

proc initYTPlot() {
    windowE = new Graph(0)   // first window for E cells 
	if (cellTy == 0) windowE.view(0, 0, tstop, 1, 700, 500, 300, 200)
    windowI = new Graph(0)   // second window for I cells 
    windowI.view(0, -80, tstop, 50, 1300, 500, 300, 200)
    if (plotFlag == 1) graphList[0].append(windowE) // add to standard graph list (plot vs t) - Carnevale and Hines, p. 180 
	if (plotFlag == 1) graphList[0].append(windowI)
}

proc showYTPlot() { local i, max
    windowE.erase_all()
	windowI.erase_all()
    if (nECells > 30) max = 30 else max = nECells // window plot for subset of E cells 
    for i = 0, (max-1) {
	  if (cellTy == 0) sprint(expression, "%s%d%s", "ECells.object(", i, ").infi.M")
      windowE.addexpr(expression, 1, 1, 0.8, 0.9, 2)   // expression, color index, brush index ... 
	}  

    if (nICells > 30) max = 30 else max = nICells // window plot for subset of I cells 
    for i = 0, (max-1) {
      sprint(expression, "%s%d%s", "ICells.object(", i, ").soma.v(0.5)") 
      windowI.addexpr(expression, 2, 1, 0.8, 0.9, 2)   // E cells in black, I cells in red 
	}  
}


func sign() { local myx // signum function; par1 = x
if($1 > 0) myx = 1 else {
  if($1 < 0) myx = -1 else myx = 0
  }
  return myx
}

objref f5, f6 // matrix files
objref v2 
v2 = new Vector()

double myVar[2]

objref connectionsEI, connectionsIE, connectionsII, vec, nc, nil
connectionsEI = new List()
connectionsIE = new List()
connectionsII = new List()

proc readMatrix() {
  eps = 0.001
  f5 = new File()  

//  if (strobj.len(pathIn) > 0) sprint(expression, "%s/matrix.in", pathIn) else sprint(expression, "matrix.in")
//   sprint(expression, "D:/User/nrn/IntegrateAndFire/sonet/matrix_recip5.txt")
  sprint(expression, "matrixFile")
  f5.ropen(expression)
  
  while (!f5.eof) {  
// 	f5.vread(2, &myVar)   read binary format 
//  print myVar[0], myVar[1]  
//  ii = int(myVar[0] + eps) // presynaptic index, full matrix 
//	jj = int(myVar[1] + eps) // postsynaptic index, full matrix 
	
	ii = f5.scanvar()  jj = f5.scanvar()    // read asci format - indices from now on should start from 0 

	if ((ii < 0) || (ii >= nICells+nECells) || (jj < 0) || (jj >= nICells+nECells)) print "Warning: I-E index does not exist !" 
	
	if ((ii >= nICells) && (ii < nICells+nECells-1) && (jj >= 0) && (jj < nICells-1)) {
	  i = ii - nICells
	  j = jj
	  if (cvWeight > 0) myJEI = jEI*randJ.repick() else myJEI = jEI
      if (cellTy == 0) connectionsEI.append(new NetCon(ECells.object(i).infi, ICells.object(j).ncE, 1, delMax*dist(i/nECells, j/nICells), myJEI)) 
	  } 
	  
	if ((ii >= 0) && (ii < nICells-1) && (jj >= nICells) && (jj < nICells+nECells-1)) {
	  i = ii
	  j = jj - nICells
	  if (cvWeight > 0) myJIE = jIE*randJ.repick() else myJIE = jIE
	  if (cellTy == 0) ICells.object(i).soma connectionsIE.append(new NetCon(&v(0.5), ECells.object(j).infi, thresholdI, delMax*dist(i/nICells, j/nECells), -myJIE)) 
      }
	  
	if ((ii >= 0) && (ii < nICells-1) && (jj >= 0) && (jj < nICells-1)) {	
      i = ii
      j = jj	
      if (cvWeight > 0) myJIIChem = jIIChem*randJ.repick() else myJIIChem = jIIChem
	  ICells.object(i).soma connectionsII.append(new NetCon(&v(0.5), ICells.object(j).ncI, thresholdI, delMax*dist(i/nICells, j/nICells), myJIIChem)) 
      } 
	  
  } 
  f5.close() // this closes the input file 
  print "E-I synapses initialized; matrix read from file ", expression
  
} // end of proc readMatrix()

objref nConnIn, nConnOut
nConnIn = new Vector(nICells, 0)   // initialize with correct size and with 0 value 
nConnOut = new Vector(nICells, 0)

proc randomMatrix() {
if (matrixWriteFlag == 1) { 
  if (strobj.len(pathOut) > 0) sprint(expression, "%s/matrix.out", pathOut) else sprint(expression, "matrix.out")
  f6 = new File()
  f6.wopen(expression)
}

for i = 0, nECells-1 {  // connect EI synapses
  for j = 0, nICells-1 {
    if (rand.repick() < cEI * pConnect(dist(i/nECells, j/nICells), sigmaEI, 0)) {  // pars: distance, sigma, mean 
	  if (cvWeight > 0) myJEI = jEI*randJ.repick() else myJEI = jEI
	  if (cellTy == 0) connectionsEI.append(new NetCon(ECells.object(i).infi, ICells.object(j).ncE, 1, delMax*dist(i/nECells, j/nICells), myJEI)) 
	  nConnIn.x[j] = nConnIn.x[j] + 1
	  if (matrixWriteFlag == 1) { 
	    myVar[0] = i + nICells myVar[1] = j 
	    f6.vwrite(2, &myVar)
//		print myVar[0], myVar[1]
	  }
    }
  }
}  
print "E-I synapses initialized" 

for i = 0, nICells-1 { // connect IE synapses 
  for j = 0, nECells-1 {
    if (rand.repick() < cIE * pConnect(dist(i/nICells, j/nECells), sigmaIE, shift)) { // pars: distance, sigma, mean 
	   if (cvWeight > 0) myJIE = jIE*randJ.repick() else myJIE = jIE
	   if (cellTy == 0) ICells.object(i).soma connectionsIE.append(new NetCon(&v(0.5), ECells.object(j).infi, thresholdI, delMax*dist(i/nICells, j/nECells), -myJIE)) 
	   nConnOut.x[i] = nConnOut.x[i] + 1
       if (matrixWriteFlag == 1) { 
	     myVar[0] = i myVar[1] = j + nICells 
	     f6.vwrite(2, &myVar)
//		 print myVar[0], myVar[1]
	   }
	}
  }
}
print "I-E synapses initialized" 

for i = 0, nICells-1 {   // chemical mutual inhibition 
  for j = 0, nICells-1 {
    if ((rand.repick() < cIIChem * pConnect(dist(i/nICells, j/nICells), sigmaIIChem, shift)) && (i != j)) { 
	   if (cvWeight > 0) myJIIChem = jIIChem*randJ.repick() else myJIIChem = jIIChem
	   ICells.object(i).soma connectionsII.append(new NetCon(&v(0.5), ICells.object(j).ncI, thresholdI, delMax*dist(i/nICells, j/nICells), myJIIChem)) 
	   if (matrixWriteFlag == 1) { 
	      myVar[0] = i myVar[1] = j 
	      f6.vwrite(2, &myVar)
//		  print myVar[0], myVar[1]
	   }
	}
  }
}
if (matrixWriteFlag == 1) f6.close()

} // end of proc randomMatrix()

print "Step 4 finished"
// -------------------------------------------------------------------------------------
// step 5 - activate inhibitory synapses externally at the beginning to simulate preceding gamma cycle using NetStim 

objref ns, inh   // inhibitory synapses at the beginning - to all E cells 
ns = new NetStim()
ns.interval = 1 // in ms 
ns.number = 1 // single event 
ns.start = 0 // stimulus at beginning of simulations; delay in ms 
ns.noise = 0 // 0 = deterministic; 1 = negexp distribution 
inh = new List()
if (cellTy == 0) for i = 0, nECells-1 inh.append(new NetCon(ns, ECells.object(i).infi, 1, 0, -10*jIE))
// source, target, threshold, delay, weight 

print "Step 5 finished"
// -------------------------------------------------------------------------------------
// step 6 - define E - I, I - E,  and I - I connections, setup spike counter, and setup simple spiking mechanism 

spikesE = new List()
spikesI = new List()

if (matrixReadFlag == 0) randomMatrix()

for i = 0, nECells-1 { // record spikes in E neurons 
  vec = new Vector()  
  if (cellTy == 0) nc = new NetCon(ECells.object(i).infi, nil, 1, 0, 0)
  nc.record(vec)
  spikesE.append(vec)
}
print "rasterplot for E cells initialized"

for i = 0, nICells-1 { // record spikes in I neurons
  vec = new Vector() 
  ICells.object(i).soma nc = new NetCon(&v(0.5), nil, thresholdI, 0, 0)
  nc.record(vec)
  spikesI.append(vec)
}
print "rasterplot for I cells initialized"

objref gap1[1000]
objref gap1[2*nICells*nICells]  // use redeclare trick as described on p. 372 of Carnevale and Hines, 2006 
counter = 0

for i = 0, nICells-1 {   // electrical gap junction coupling 
  for j = 0, nICells-1 {
    if ((rand.repick() < cIIGap* pConnect(dist(i/nICells, j/nICells), sigmaIIGap, 0)) && (i > j)) { 
	   ICells.object(i).soma gap1[counter] = new gap(0.5) 
	   ICells.object(j).soma gap1[counter+1] = new gap(0.5)
	   gap1[counter].r = gapResistance // in MegaOhm 
	   gap1[counter+1].r = gapResistance
	   setpointer gap1[counter].vgap, ICells.object(j).soma.v(0.5)
	   setpointer gap1[counter+1].vgap, ICells.object(i).soma.v(0.5)
	   counter = counter + 2
    }
  }
}

if (matrixReadFlag == 1) readMatrix()
print "I-I synapses (chemical and electrical) initialized"


objref vec, nc // destroy objects 
print "Step 6 finished"
// -------------------------------------------------------------------------------------
// step 7 - define additional procedures and provide graphic windows

objref drive   // generate drive values from the binary activity pattern 
drive = new Vector(nECells, 0) // fill with 0 

objref pattern // generate binary random pattern 
pattern = new Vector(nECells, 0)

proc printIterationInfo() {
  sumE1 = 0
  sumE2 = 0 
  for i = 0, nECells-1 sumE1 = sumE1 + spikesE.object(i).size // number of spikes 
  for i = 0, nECells-1 sumE2 = sumE2 + sign(spikesE.object(i).size) // number of firing cells 
  sumI1 = 0
  sumI2 = 0   
  for i = 0, nICells-1 sumI1 = sumI1 + spikesI.object(i).size // number of spikes 
  for i = 0, nICells-1 sumI2 = sumI2 + sign(spikesI.object(i).size) // number of firing cells 
  
  for i = 0, nECells-1 pattern.x[i] = sign(spikesE.object(i).size) // binary pattern (0 = no firing, 1 = firing)
  
  sum = 0
  count = 0 
  for i = 0, nECells-1 if (spikesE.object(i).size > 0) { sum = sum + spikesE.object(i).x[0] count = count + 1  }  // first spike only 
  if (count > 0) spikeTimeE = sum / count else spikeTimeE= 0 

  sum = 0
  count = 0 
  for i = 0, nICells-1 if (spikesI.object(i).size > 0) { sum = sum + spikesI.object(i).x[0] count = count + 1  }  // first spike only 
  if (count > 0) spikeTimeI = sum / count else spikeTimeI= 0 

  
  arg = drive.dot(drive) * pattern.dot(pattern)  // drive-based correlation 
  if (arg > 0) r2 = drive.dot(pattern) / sqrt(arg) else r2 = 0  
  // see Wang and Buzsaki, 1996

  print "Iteration = ", iteration 
  print "Sum all E cell firings = ", sumE1, "sum of firing E cells = ", sumE2, "drive-based cc = ", r2
  print "Sum all I cell firings = ", sumI1, "sum of firing I cells = ", sumI2, "drive-based cc = ", r2
  print "Mean firing time E cells = ", spikeTimeE, "Mean firing time I cells = ", spikeTimeI 
  print "----------------------------------------------------------------------------------"
} 

proc printNetworkInfo() {
  print "Number of ECells = ", ECells.count, " Number of ICells = ", ICells.count
  print "Number of EI connections = ", connectionsEI.count, " Number of IE connections = ", connectionsIE.count,  "Number of II connections = ", connectionsII.count
  print "Number of input connections: ", nConnIn.mean, " +- ", nConnIn.stdev, " min: ", nConnIn.min, " (max: ", nConnIn.max, ")"
  print "Number of output connections: ", nConnOut.mean, " +- ", nConnOut.stdev, " (min: ", nConnOut.min, " max: ", nConnOut.max, ")"  
  print "----------------------------------------------------------------------------------"
} 

proc advance() {
	fadvance()
	if ((t < tstop-dt) && (fileFlag == 1)) {
	  f3.printf("%g ", t) // print time 
	  for (i=0; i<nICells; i=i+nIStep) f3.printf("%g ", ICells.object(i).soma.v(0.5))              // print I Cell voltage
	  if (cellTy == 0) for (i=0; i<nECells; i=i+nEStep) f3.printf("%g ", ECells.object(i).infi.M)  // print E Cell voltage or M 
	  f3.printf(" \n ")
	  }
	
	/* write output to file voltage.out */
	/* t in ms, voltage at soma in mV, spacing = dt */
}

if (plotFlag == 1) load_file("nrngui.hoc")
if (plotFlag == 0) load_file("stdrun.hoc")

if (plotFlag == 1) nrncontrolmenu() // RunControl menue 
if (plotFlag == 1) initRasterPlot2() 
if (plotFlag == 1) initYTPlot() 

print "Step 7 finished"
  
// Run main loop here 
// -------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------

printNetworkInfo()
for iteration = 1, 100 { // 100 patterns 

if (fileFlag == 1) {
  f3 = new File()                                         
   if (strobj.len(pathOut) > 0) sprint(expression, "%s/voltage_%d.out", pathOut, iteration) else sprint(expression, "voltage_%d.out", iteration)
  f3.wopen(expression)
  }
  if (strobj.len(pathIn) > 0) sprint(expression, "%s/drive_%d.in", pathIn, iteration) else sprint(expression, "drive_%d.in", iteration)  // Read the random excitatory drive from drive.in file generated by MMA or Python  
  f1 = new File()
  print "Read in file ", expression
  
  f1.ropen(expression)
  i = 0
  while ((!f1.eof) && (i < nECells)) {drive.x[i] = driveScale * f1.scanvar()
    i = i + 1
  } 
  f1.close() // this closes the input file 

  if (cellTy == 0) for j = 0, nECells-1 ECells.object(j).infi.ib = drive.x[j] 

  if (plotFlag == 1) showYTPlot()                           // show YT plots - before run to establish continuous plots 
  
  run()                                                     // run simulation 
  
  if (plotFlag == 1) showRasterPlot2()                      // show raster plots 
  if (fileFlag == 1) outputRasterPlot2()                    // write output to files 
  printIterationInfo()                                      // analyze results and print info 
  
  f2 = new File()                                           // Write pattern to pattern.out file 
  if (strobj.len(pathOut) > 0) sprint(expression, "%s/pattern_%d.out", pathOut, iteration) else sprint(expression, "pattern_%d.out", iteration)
  f2.wopen(expression)
  for i = 0, nECells-1 f2.printf("%g \n", pattern.x[i])
  f2.close() // this closes the output file
  if (fileFlag == 1) f3.close()
}
// -------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------

quit() // use only for running the hoc file from mma or python






