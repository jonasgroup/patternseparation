// Pattern separation network based on mixed integrate and fire / passive - active conductance based neurons 
// See Neuron book, p. 289 ff. 
// PJ, 01.02.2017
// Version 15 - Hybrid conductance and IF
// jIE jII positive - conversion into integrate and fire requires negative sign in the NetCon

dt = 0.025  // Same dt as in WB models 
steps_per_ms = 10 // for plotting 
celsius = 6.3 // to use the WB model properly 

strdef pathIn, pathOut 
pathIn = "<*pathIn*>"
pathOut = "<*pathOut*>"
driveScale = 1 // factor to scale excitatory drive - 1 by default 

// Variable time step - optional 
// cvode_active(1)   // cvode_active(1) activates variable time step 
// cvode.atol(0.001) // sets absolute tolerance for variable time step  
// cvode.use_local_dt(1) // use local dt for individual cells 
// cvode.current_method() // Should be: 1301, 1 for local, 3 for cvode, and 0 1 

// Splice parameters for full size: 500000, 5000, 0.1, 0.03, 0.3, 0.06, 0.6, 0.03  
// Splice parameters for test size: 10000, 100, 0.1, 0.1, 0.3, 0.25, 0.8, 0.05 

nECells = <*nE*> // 500000    // numbers of E cells and I cells (10000 and 100 for test purposes)  
nICells = <*nI*> // 5000
driveScale = <*driveScale*> 

tstop = 50 // 50 - max simulation time in ms 
delMax = <*delayMax*> // 10 - synaptic latency in ms 
// ---------------------------------------------------------------
cEI = <*cEI*> // 0.1 - peak connection probability for E to I synapses 
sigmaEI = <*sigmaEI*> // 0.03 - standard deviation of connection probability for E to I synapses 
jEI = <*jEI*> // 0.008 // synaptic weight for E to I synapses in �S 
// ---------------------------------------------------------------
cIE = <*cIE*> // 0.3 - dto for I to E synapses 
sigmaIE = <*sigmaIE*> // 0.06
jIE = 0.2 //  
shift = <*shift*> // 0 - shift of peak of inhibitory connection probability 
// ---------------------------------------------------------------
cIIChem = <*cIE*> // 0.3 - peak connection probability for I to I synapses 
sigmaIIChem = <*sigmaIE*> // 0.06 - standard deviation of connection probability for I to I synapses
jIIChem = <*jII*> // 0.02 // synaptic weight for I to I synapses
// ---------------------------------------------------------------
cIIGap = <*cGap*> // 0.6 - peak connection probability for gap junctions
gapResistance = 300 // 300 - gap junction transcellular resistance in MegaOhm 
sigmaIIGap = <*sigmaGap*> // 0.03 - standard deviation of connection probability for gap junctions
// ---------------------------------------------------------------
jAHP = 0.5      // 0.5 - AHP conductance, e.g. 0.5 �S
rSeed = 10      // seed for random number generators
// ---------------------------------------------------------------
cellTy = 0      // Integrate and fire models for excitatory cells   
plotFlag = 1    //  0 = no GUI windows, 1 = GUI windows  
fileFlag = 1    // 0 = dont write output to file 1 = write output to file 

objref f1, f2, f3, f4 // files 
thresholdE = -50 // threshold in mV for E neurons 
thresholdI = 0 // threshold in mV - overshoot for I neurons 
// -------------------------------------------------------------------------------------
// step 1 - define templates (classes)

begintemplate ECell // ECells as integrate-and-fire models
  public infi
  objref infi
  proc init() {
    infi = new IntFire2()
    infi.taum = 20 // membrane time constant in ms 
    infi.taus = 30 // synaptic time constant in ms 
    infi.ib = 1  // constant current input 
  }
endtemplate ECell

// Note: taus > taum, otherwise error messages like this Intfire2 iter 11 x=0.900568 f=1 df=-0.0861853 dx=1.97865e-011 a=2.38732 b=436.443 c=-437.832 r=1.00333

/* begintemplate ECell2
  public soma, ncI, ncAHP, stim1
  create soma
  objref ncI, ncAHP, stim1

  proc init() {
    soma { nseg = 1 Ra = 200 diam = 50 L = 50 } // to give right input resistance and membrane time constant - Ra not needed 
	vRest = -70 // in mV 
    forall { insert pas // insert passive conductance 
	v = vRest 
    g_pas = 0.00005 // in S cm-2; gives 125 MOhm membrane resistance 
    cm = 1.0  // in �F cm-2; gives 50 ms membrane time constant 	
	e_pas = vRest }
	access soma
	ncI = new Exp2Syn(0.5)
    ncI.tau1 = 0.1
    ncI.tau2 = 5
    ncI.e = -75
	ncAHP = new ExpSyn(0.5) // instantaneous rise 
    ncAHP.tau = 17
    ncAHP.e = -90
	stim1 = new IClamp(0.5)  // stim1 is for excitatory drive 
	stim1.del = 0
	stim1.dur = 1e+06
	stim1.amp = 0 // in nA 
  }
endtemplate ECell2 */


begintemplate ICell2   // I cells as conductance based models
  public soma, ncE, ncI
  create soma
  objref ncE, ncI

  proc init() {
    soma { nseg = 1 Ra = 200 diam = 70 L = 70 } // to give reasonable input resistance and membrane time constant - Ra not needed 
	vRest = -65 // in mV 
    forall { 
	v = vRest // initialize resting potential 
    cm = 1.0  // in �F cm-2; gives 10 ms time constant 	
	insert hh_w   // insert active conductances 
	gl_hh_w = 0.0001 // im S cm-2
	el_hh_w = -65.7116 // reversal potential of leak conductance = resting potential
	}
	access soma
    ncE = new Exp2Syn(0.5) // exponential rise 
    ncE.tau1 = 0.1
	ncE.tau2 = 1  // fast excitation at BC-BC synapses 
    ncE.e = 0
	ncI = new Exp2Syn(0.5)
    ncI.tau1 = 0.1
    ncI.tau2 = 2.5 // fast inhibition at BC-BC synapses 
    ncI.e = -65 // shunting inhition
  }
endtemplate ICell2

print "Step 1 finished"
// -------------------------------------------------------------------------------------
// step 2 - generate cells (objects) 

objref ECells, ICells 
ECells = new List()
ICells = new List()
for i = 0, nECells-1 ECells.append(new ECell())    // use integrate and fire 
for i = 0, nICells-1 ICells.append(new ICell2()) 

print "Step 2 finished"
// -------------------------------------------------------------------------------------
// step 3 - initialize random number generators  

objref rand                     // generate simple uniform random numbers between 0 and 1  
rand = new Random(rSeed)
rand.uniform(0, 1)               

// use repick from now on, faster 

print "Step 3 finished"
// -------------------------------------------------------------------------------------
// step 4 - define functions and procedures   

func dist() { local myDist      // calculate distance between 2 neurons; par1 = first index, par2 = last index (normalized)
  myDist = abs($1 - $2)
  if(myDist < 0.5) return myDist else return abs(myDist-1)   // close to implement ring 
}

func pConnect() { local distance, sigma, value, mu  // normal distribution = Gaussian, peak normalized to 1 
  distance = $1
  sigma = $2
  mu = $3
  value = exp(-(distance - mu)^2 / (2*sigma^2))   
  return value 
}

slope = 0.01
func pConnect2() { local distance, mu, slope      // sigmoidal function = logistic, peak 1 
  distance = abs($1)
  mu = $2
//  slope = $3
  
  value = 1 / (1 + exp((distance - mu) / slope))   
  return value 
}

objref spikey, spikesE, spikesI
objref gRasterE, gRasterI

proc initRasterPlot2() {
  gRasterE = new Graph(0) // 0 = allow for size and place 
  gRasterE.view(0, 0, tstop, nECells, 700, 100, 300, 200) // last four pars: left, top, width, height
  gRasterI = new Graph(0)
  gRasterI.view(0, 0, tstop, nICells, 1300, 100, 300, 200) 
}

proc showRasterPlot2() { localobj spikes  // shows raster plot; par1 = spikesE or I, par2 = color (1 = black, 2 = red, 3 = blue, 4 = green, etc., par3 = shift
  gRasterE.erase_all()
  gRasterI.erase_all()
  for i = 0, spikesE.count()-1 {
    spikey = spikesE.object(i).c // create a vector that has as many elements as the number of spikes the cell fired
    spikey.fill(i+1) // fill those elements with an integer that is 1 larger than the ordinal position of that cell
    spikey.mark(gRasterE, spikesE.object(i), "O", 5, 1) // plot filled circles of size 5 and color 1 = black - vector class, not Graphics
  }
  for i = 0, spikesI.count()-1 {
    spikey = spikesI.object(i).c // create a vector that has as many elements as the number of spikes the cell fired
    spikey.fill(i+1) // fill those elements with an integer that is 1 larger than the ordinal position of that cell
    spikey.mark(gRasterI, spikesI.object(i), "O", 5, 2) // plot filled circles of size 5 and color 2 = red ...   - vector class, not Graphics
  }
} 

strdef expression

proc outputRasterPlot2() {
  f4 = new File()
  sprint(expression, "%sraster.out", pathOut) 
  f4.wopen(expression)

  for i = 0, nICells-1 for j = 0, spikesI.object(i).size-1 f4.printf("ICells  %g %g \n", i+1, spikesI.object(i).x[j]) 
  for i = 0, nECells-1 for j = 0, spikesE.object(i).size-1 f4.printf("ECells %g %g \n", i+1, spikesE.object(i).x[j])
  f4.close()
}

objectvar windowE, windowI  // window plot for subset of E cells 

proc initYTPlot() {
    windowE = new Graph(0)   // first window for E cells 
//  if (cellTy == 1) windowE.view(0, -80, tstop, 50, 700, 500, 300, 200)
	if (cellTy == 0) windowE.view(0, 0, tstop, 1, 700, 500, 300, 200)
    windowI = new Graph(0)   // second window for I cells 
    windowI.view(0, -80, tstop, 50, 1300, 500, 300, 200)
    if (plotFlag == 1) graphList[0].append(windowE) // add to standard graph list (plot vs t) - Carnevale and Hines, p. 180 
	if (plotFlag == 1) graphList[0].append(windowI)
}

proc showYTPlot() { local i, max
    windowE.erase_all()
	windowI.erase_all()
    if (nECells > 30) max = 30 else max = nECells // window plot for subset of E cells 
    for i = 0, (max-1) {
      if (cellTy == 1) sprint(expression, "%s%d%s", "ECells.object(", i, ").soma.v(0.5)") 
	  if (cellTy == 0) sprint(expression, "%s%d%s", "ECells.object(", i, ").infi.M")
      windowE.addexpr(expression, 1, 1, 0.8, 0.9, 2)   // expression, color index, brush index ... 
	}  

    if (nICells > 30) max = 30 else max = nICells // window plot for subset of I cells 
    for i = 0, (max-1) {
      sprint(expression, "%s%d%s", "ICells.object(", i, ").soma.v(0.5)") 
      windowI.addexpr(expression, 2, 1, 0.8, 0.9, 2)   // E cells in black, I cells in red 
	}  
}


func sign() { local myx // signum function; par1 = x
if($1 > 0) myx = 1 else {
  if($1 < 0) myx = -1 else myx = 0
  }
  return myx
}

print "Step 4 finished"
// -------------------------------------------------------------------------------------
// step 5 - activate inhibitory synapses externally at the beginning to simulate preceding gamma cycle using NetStim 

objref ns, inh   // inhibitory synapses at the beginning - to all E cells 
ns = new NetStim()
ns.interval = 1 // in ms 
ns.number = 1 // single event 
ns.start = 0 // stimulus at beginning of simulations; delay in ms 
ns.noise = 0 // 0 = deterministic; 1 = negexp distribution 
inh = new List()
// if (cellTy == 1) for i = 0, nECells-1 inh.append(new NetCon(ns, ECells.object(i).ncI, 1, 0, 10*jIE))
if (cellTy == 0) for i = 0, nECells-1 inh.append(new NetCon(ns, ECells.object(i).infi, 1, 0, -10*jIE))
// source, taget, threshold, delay, weight 

print "Step 5 finished"
// -------------------------------------------------------------------------------------
// step 7 - define E - I and I - E connections, setup spike counter, and setup simple spiking mechanism including AHP    

objref connections, vec, nc, nil, ncRefract 
connections = new List()
ncRefract = new List()
spikesE = new List()
spikesI = new List()

for i = 0, nECells-1 {  // connect EI synapses
  for j = 0, nICells-1 {
    if (rand.repick() < cEI * pConnect(dist(i/nECells, j/nICells), sigmaEI, 0)) {  // pars: distance, sigma, mean 
// 	  if (cellTy == 1) ECells.object(i).soma connections.append(new NetCon(&v(0.5), ICells.object(j).ncE, thresholdE, delMax*dist(i/nECells, j/nICells), jEI)) 
	  if (cellTy == 0) connections.append(new NetCon(ECells.object(i).infi, ICells.object(j).ncE, 1, delMax*dist(i/nECells, j/nICells), jEI)) 
    }
  }
  vec = new Vector()  // record spikes in E neurons 
//  if (cellTy == 1) ECells.object(i).soma nc = new NetCon(&v(0.5), nil, thresholdE, 0, 0)
//  if (cellTy == 1) ECells.object(i).soma ncRefract.append(new NetCon(&v(0.5), ECells.object(i).ncAHP, thresholdE, 0, jAHP))
  if (cellTy == 0) nc = new NetCon(ECells.object(i).infi, nil, 1, 0, 0)
//  if (cellTy == 0) ncRefract.append(new NetCon(ECells.object(i).infi, ECells.object(i).infi, 1, 0, -jAHP))
  nc.record(vec)
  spikesE.append(vec)
}
print "E-I synapses initialized"

for i = 0, nICells-1 { // connect IE synapses 
  for j = 0, nECells-1 {
    if (rand.repick() < cIE * pConnect(dist(i/nICells, j/nECells), sigmaIE, shift)) { // pars: distance, sigma, mean 
//	   if (cellTy == 1) ICells.object(i).soma connections.append(new NetCon(&v(0.5), ECells.object(j).ncI, thresholdI, delMax*dist(i/nICells, j/nECells), jIE)) 
	   if (cellTy == 0) ICells.object(i).soma connections.append(new NetCon(&v(0.5), ECells.object(j).infi, thresholdI, delMax*dist(i/nICells, j/nECells), -jIE)) 
	  }
  }
  vec = new Vector()  // record spikes in I neurons
  ICells.object(i).soma nc = new NetCon(&v(0.5), nil, thresholdI, 0, 0)
  nc.record(vec)
  spikesI.append(vec)
}
print "I-E synapses initialized"

objref gap1[1000]
objref gap1[nICells*nICells]  // use redeclare trick as described on p. 372 of Carnevale and Hines, 2006 
counter = 0

for i = 0, nICells-1 { 
  for j = 0, nICells-1 {
    if (rand.repick() < cIIChem * pConnect(dist(i/nICells, j/nICells), sigmaIIChem, shift)) { // chemical mutual inhibition 
	   ICells.object(i).soma connections.append(new NetCon(&v(0.5), ICells.object(j).ncI, thresholdI, delMax*dist(i/nICells, j/nICells), jIIChem)) 
	   }

    if (rand.repick() < cIIGap* pConnect(dist(i/nICells, j/nICells), sigmaIIGap, 0)) { // electrical gap junction coupling 
	   ICells.object(i).soma gap1[counter] = new gap(0.5) 
	   ICells.object(j).soma gap1[counter+1] = new gap(0.5)
	   gap1[counter].r = gapResistance // in MegaOhm 
	   gap1[counter+1].r = gapResistance
	   setpointer gap1[counter].vgap, ICells.object(j).soma.v(0.5)
	   setpointer gap1[counter+1].vgap, ICells.object(i).soma.v(0.5)
	   counter = counter + 2
    }
  }
}
print "I-I synapses initialized"
objref vec, nc // destroy objects 
print "Step 6 finished"
// -------------------------------------------------------------------------------------
// provide graphic windows, do some counts, calculate correlation parameters     

objref drive   // generate drive values from the binary activity pattern 
drive = new Vector(nECells, 0) // fill with 0 

objref pattern // generate binary random pattern 
pattern = new Vector(nECells, 0)

proc analyzeAndPrint() {
  sum1 = 0
  sum2 = 0 
  for i = 0, nECells-1 sum1 = sum1 + spikesE.object(i).size // number of spikes 
  for i = 0, nECells-1 sum2 = sum2 + sign(spikesE.object(i).size) // number of firing cells 
  for i = 0, nECells-1 pattern.x[i] = sign(spikesE.object(i).size) // binary pattern (0 = no firing, 1 = firing)

  arg = drive.dot(drive) * pattern.dot(pattern)  // drive-based correlation 
  if (arg > 0) r2 = drive.dot(pattern) / sqrt(arg) else r2 = 0  
  // see Wang and Buzsaki, 1996

  print "Iteration = ", iteration 
  print "total number of ECells = ", ECells.count, " Total number of ICells = ", ICells.count, " Total number of connections = ", connections.count 
  print "sum all firings = ", sum1, "sum of firing cells = ", sum2, "drive-based cc = ", r2
} 


proc advance() {
	fadvance()
	if ((t < tstop-dt) && (fileFlag == 1)) {
	  f3.printf("%g ", t) // print time 
	  for i = 0, 29 f3.printf("%g ", ICells.object(i).soma.v(0.5))  // print I Cell voltage
	  if (cellTy == 1) for i = 0, 29 f3.printf("%g ", ECells.object(i).soma.v(0.5))  // print E Cell voltage or M
	  if (cellTy == 0) for i = 0, 29 f3.printf("%g ", ECells.object(i).infi.M)
	  f3.printf(" \n ")
	  }
	
	/* write output to file outfile.1 */
	/* t in ms, voltage at soma in mV, voltage at distal dendrite in mV, spacing = dt */
}

if (plotFlag == 1) load_file("nrngui.hoc")
if (plotFlag == 0) load_file("stdrun.hoc")

if (plotFlag == 1) nrncontrolmenu() // RunControl menue 
if (plotFlag == 1) initRasterPlot2() 
if (plotFlag == 1) initYTPlot() 

  
// Run main loop here 
// -------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------
for iteration = 1, 100 { // 100 patterns 

if (fileFlag == 1) {
  f3 = new File()                                         
  sprint(expression, "%svoltage.out", pathOut) 
  f3.wopen(expression)
  }
  
  sprint(expression, "%s%s%d%s", pathIn, "drive_", iteration, ".in")  // Read the random excitatory drive from drive.in file generated by MMA or Python  
  f1 = new File()
  f1.ropen(expression)
  i = 0
  while ((!f1.eof) && (i < nECells)) {drive.x[i] = driveScale * f1.scanvar()
    i = i + 1
  } 
  f1.close() // this closes the input file 

  // if (cellTy == 1) for j = 0, nECells-1 ECells.object(j).stim1.amp = drive.x[j] // Apply excitatory drive 
  if (cellTy == 0) for j = 0, nECells-1 ECells.object(j).infi.ib = drive.x[j] 

  if (plotFlag == 1) showYTPlot()                           // show YT plots - before run to establish continuous plots 
  
  run()                                                     // run simulation 
  
  if (plotFlag == 1) showRasterPlot2()                      // show raster plots 
  if (fileFlag == 1) outputRasterPlot2()                    // write output to files 
  analyzeAndPrint()                                         // analyze results 
  
  f2 = new File()                                           // Write pattern to pattern.out file 
  sprint(expression, "%s%s%d%s", pathOut, "pattern_", iteration, ".out") 
  f2.wopen(expression)
  for i = 0, nECells-1 f2.printf("%g \n", pattern.x[i])
  f2.close() // this closes the output file
  if (fileFlag == 1) f3.close()
}
// -------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------

quit() // use only for running the hoc file from mma or python






