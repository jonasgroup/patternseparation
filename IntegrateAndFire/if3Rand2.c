/* Created by Language version: 7.7.0 */
/* NOT VECTORIZED */
#define NRN_VECTORIZED 0
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "scoplib_ansi.h"
#undef PI
#define nil 0
#include "md1redef.h"
#include "section.h"
#include "nrniv_mf.h"
#include "md2redef.h"
 
#if METHOD3
extern int _method3;
#endif

#if !NRNGPU
#undef exp
#define exp hoc_Exp
extern double hoc_Exp(double);
#endif
 
#define nrn_init _nrn_init__IF3Rand2
#define _nrn_initial _nrn_initial__IF3Rand2
#define nrn_cur _nrn_cur__IF3Rand2
#define _nrn_current _nrn_current__IF3Rand2
#define nrn_jacob _nrn_jacob__IF3Rand2
#define nrn_state _nrn_state__IF3Rand2
#define _net_receive _net_receive__IF3Rand2 
#define factors factors__IF3Rand2 
#define newstates newstates__IF3Rand2 
#define seed seed__IF3Rand2 
#define update update__IF3Rand2 
 
#define _threadargscomma_ /**/
#define _threadargsprotocomma_ /**/
#define _threadargs_ /**/
#define _threadargsproto_ /**/
 	/*SUPPRESS 761*/
	/*SUPPRESS 762*/
	/*SUPPRESS 763*/
	/*SUPPRESS 765*/
	 extern double *getarg();
 static double *_p; static Datum *_ppvar;
 
#define t nrn_threads->_t
#define dt nrn_threads->_dt
#define taue _p[0]
#define taui _p[1]
#define taum _p[2]
#define s _p[3]
#define bTest _p[4]
#define bOffset _p[5]
#define e _p[6]
#define i _p[7]
#define m _p[8]
#define nself _p[9]
#define nexcite _p[10]
#define ninhibit _p[11]
#define b _p[12]
#define k _p[13]
#define enew _p[14]
#define inew _p[15]
#define mnew _p[16]
#define t0 _p[17]
#define ae _p[18]
#define ai _p[19]
#define be _p[20]
#define bi _p[21]
#define on _p[22]
#define _tsav _p[23]
#define _nd_area  *_ppvar[0]._pval
 
#if MAC
#if !defined(v)
#define v _mlhv
#endif
#if !defined(h)
#define h _mlhh
#endif
#endif
 
#if defined(__cplusplus)
extern "C" {
#endif
 static int hoc_nrnpointerindex =  -1;
 /* external NEURON variables */
 /* declaration of user functions */
 static double _hoc_E();
 static double _hoc_I();
 static double _hoc_M();
 static double _hoc_firetimebound();
 static double _hoc_factors();
 static double _hoc_newstates();
 static double _hoc_seed();
 static double _hoc_tf();
 static double _hoc_update();
 static int _mechtype;
extern void _nrn_cacheloop_reg(int, int);
extern void hoc_register_prop_size(int, int, int);
extern void hoc_register_limits(int, HocParmLimits*);
extern void hoc_register_units(int, HocParmUnits*);
extern void nrn_promote(Prop*, int, int);
extern Memb_func* memb_func;
 
#define NMODL_TEXT 1
#if NMODL_TEXT
static const char* nmodl_file_text;
static const char* nmodl_filename;
extern void hoc_reg_nmodl_text(int, const char*);
extern void hoc_reg_nmodl_filename(int, const char*);
#endif

 extern Prop* nrn_point_prop_;
 static int _pointtype;
 static void* _hoc_create_pnt(_ho) Object* _ho; { void* create_point_process();
 return create_point_process(_pointtype, _ho);
}
 static void _hoc_destroy_pnt();
 static double _hoc_loc_pnt(_vptr) void* _vptr; {double loc_point_process();
 return loc_point_process(_pointtype, _vptr);
}
 static double _hoc_has_loc(_vptr) void* _vptr; {double has_loc_point();
 return has_loc_point(_vptr);
}
 static double _hoc_get_loc_pnt(_vptr)void* _vptr; {
 double get_loc_point_process(); return (get_loc_point_process(_vptr));
}
 extern void _nrn_setdata_reg(int, void(*)(Prop*));
 static void _setdata(Prop* _prop) {
 _p = _prop->param; _ppvar = _prop->dparam;
 }
 static void _hoc_setdata(void* _vptr) { Prop* _prop;
 _prop = ((Point_process*)_vptr)->_prop;
   _setdata(_prop);
 }
 /* connect user functions to hoc names */
 static VoidFunc hoc_intfunc[] = {
 0,0
};
 static Member_func _member_func[] = {
 "loc", _hoc_loc_pnt,
 "has_loc", _hoc_has_loc,
 "get_loc", _hoc_get_loc_pnt,
 "E", _hoc_E,
 "I", _hoc_I,
 "M", _hoc_M,
 "firetimebound", _hoc_firetimebound,
 "factors", _hoc_factors,
 "newstates", _hoc_newstates,
 "seed", _hoc_seed,
 "tf", _hoc_tf,
 "update", _hoc_update,
 0, 0
};
#define E E_IF3Rand2
#define I I_IF3Rand2
#define M M_IF3Rand2
#define firetimebound firetimebound_IF3Rand2
#define tf tf_IF3Rand2
 extern double E( );
 extern double I( );
 extern double M( );
 extern double firetimebound( );
 extern double tf( double );
 /* declare global and static user variables */
#define eps eps_IF3Rand2
 double eps = 1e-006;
#define refrac refrac_IF3Rand2
 double refrac = 5;
 /* some parameters have upper and lower limits */
 static HocParmLimits _hoc_parm_limits[] = {
 "s", 0, 1,
 "taum", 1e-009, 1e+009,
 "taui", 1e-009, 1e+009,
 "taue", 1e-009, 1e+009,
 0,0,0
};
 static HocParmUnits _hoc_parm_units[] = {
 "refrac_IF3Rand2", "ms",
 "taue", "ms",
 "taui", "ms",
 "taum", "ms",
 "s", "1",
 0,0
};
 static double v = 0;
 /* connect global user variables to hoc */
 static DoubScal hoc_scdoub[] = {
 "eps_IF3Rand2", &eps_IF3Rand2,
 "refrac_IF3Rand2", &refrac_IF3Rand2,
 0,0
};
 static DoubVec hoc_vdoub[] = {
 0,0,0
};
 static double _sav_indep;
 static void nrn_alloc(Prop*);
static void  nrn_init(_NrnThread*, _Memb_list*, int);
static void nrn_state(_NrnThread*, _Memb_list*, int);
 static void _hoc_destroy_pnt(_vptr) void* _vptr; {
   destroy_point_process(_vptr);
}
 /* connect range variables in _p that hoc is supposed to know about */
 static const char *_mechanism[] = {
 "7.7.0",
"IF3Rand2",
 "taue",
 "taui",
 "taum",
 "s",
 "bTest",
 "bOffset",
 0,
 "e",
 "i",
 "m",
 "nself",
 "nexcite",
 "ninhibit",
 "b",
 "k",
 0,
 0,
 0};
 
extern Prop* need_memb(Symbol*);

static void nrn_alloc(Prop* _prop) {
	Prop *prop_ion;
	double *_p; Datum *_ppvar;
  if (nrn_point_prop_) {
	_prop->_alloc_seq = nrn_point_prop_->_alloc_seq;
	_p = nrn_point_prop_->param;
	_ppvar = nrn_point_prop_->dparam;
 }else{
 	_p = nrn_prop_data_alloc(_mechtype, 24, _prop);
 	/*initialize range parameters*/
 	taue = 3;
 	taui = 10;
 	taum = 30;
 	s = 0;
 	bTest = 0;
 	bOffset = 0;
  }
 	_prop->param = _p;
 	_prop->param_size = 24;
  if (!nrn_point_prop_) {
 	_ppvar = nrn_prop_datum_alloc(_mechtype, 3, _prop);
  }
 	_prop->dparam = _ppvar;
 	/*connect ionic variables to this model*/
 
}
 static void _initlists();
 
#define _tqitem &(_ppvar[2]._pvoid)
 static void _net_receive(Point_process*, double*, double);
 extern Symbol* hoc_lookup(const char*);
extern void _nrn_thread_reg(int, int, void(*)(Datum*));
extern void _nrn_thread_table_reg(int, void(*)(double*, Datum*, Datum*, _NrnThread*, int));
extern void hoc_register_tolerance(int, HocStateTolerance*, Symbol***);
extern void _cvode_abstol( Symbol**, double*, int);

 void _if3Rand2_reg() {
	int _vectorized = 0;
  _initlists();
 	_pointtype = point_register_mech(_mechanism,
	 nrn_alloc,(void*)0, (void*)0, (void*)0, nrn_init,
	 hoc_nrnpointerindex, 0,
	 _hoc_create_pnt, _hoc_destroy_pnt, _member_func);
 _mechtype = nrn_get_mechtype(_mechanism[1]);
     _nrn_setdata_reg(_mechtype, _setdata);
 #if NMODL_TEXT
  hoc_reg_nmodl_text(_mechtype, nmodl_file_text);
  hoc_reg_nmodl_filename(_mechtype, nmodl_filename);
#endif
  hoc_register_prop_size(_mechtype, 24, 3);
  hoc_register_dparam_semantics(_mechtype, 0, "area");
  hoc_register_dparam_semantics(_mechtype, 1, "pntproc");
  hoc_register_dparam_semantics(_mechtype, 2, "netsend");
 add_nrn_artcell(_mechtype, 2);
 add_nrn_has_net_event(_mechtype);
 pnt_receive[_mechtype] = _net_receive;
 pnt_receive_size[_mechtype] = 1;
 	hoc_register_var(hoc_scdoub, hoc_vdoub, hoc_intfunc);
 	ivoc_help("help ?1 IF3Rand2 C:/user/nrn/IntegrateAndFire/if3Rand2.mod\n");
 hoc_register_limits(_mechtype, _hoc_parm_limits);
 hoc_register_units(_mechtype, _hoc_parm_units);
 }
 static double _ztotal , _znexttotal ;
static int _reset;
static char *modelname = "";

static int error;
static int _ninits = 0;
static int _match_recurse=1;
static void _modl_cleanup(){ _match_recurse=1;}
static int factors();
static int newstates(double);
static int seed(double);
static int update();
 
static int  newstates (  double _ld ) {
   double _lee , _lei , _lem ;
 _lee = exp ( - _ld / taue ) ;
   _lei = exp ( - _ld / taui ) ;
   _lem = exp ( - _ld / taum ) ;
   enew = e * _lee ;
   inew = i * _lei ;
   mnew = b + ( m - b ) * _lem + ae * e * ( taue / ( taum - taue ) ) * ( _lem - _lee ) + ai * i * ( taui / ( taum - taui ) ) * ( _lem - _lei ) ;
    return 0; }
 
static double _hoc_newstates(void* _vptr) {
 double _r;
    _hoc_setdata(_vptr);
 _r = 1.;
 newstates (  *getarg(1) );
 return(_r);
}
 
double M (  ) {
   double _lM;
 if ( on ) {
     newstates ( _threadargscomma_ t - t0 ) ;
     _lM = mnew ;
     }
   else {
     _lM = 0.0 ;
     }
   
return _lM;
 }
 
static double _hoc_M(void* _vptr) {
 double _r;
    _hoc_setdata(_vptr);
 _r =  M (  );
 return(_r);
}
 
double E (  ) {
   double _lE;
 newstates ( _threadargscomma_ t - t0 ) ;
   _lE = enew ;
   
return _lE;
 }
 
static double _hoc_E(void* _vptr) {
 double _r;
    _hoc_setdata(_vptr);
 _r =  E (  );
 return(_r);
}
 
double I (  ) {
   double _lI;
 newstates ( _threadargscomma_ t - t0 ) ;
   _lI = inew ;
   
return _lI;
 }
 
static double _hoc_I(void* _vptr) {
 double _r;
    _hoc_setdata(_vptr);
 _r =  I (  );
 return(_r);
}
 
static int  update (  ) {
   e = enew ;
   i = inew ;
   m = mnew ;
   t0 = t ;
    return 0; }
 
static double _hoc_update(void* _vptr) {
 double _r;
    _hoc_setdata(_vptr);
 _r = 1.;
 update (  );
 return(_r);
}
 
static int  factors (  ) {
   double _ltp ;
 if ( taue >= taui ) {
     taui = taue + 0.01 ;
     }
   if ( taum  == taue ) {
     taum = taue + 0.01 ;
     }
   if ( taum  == taui ) {
     taum = taui + 0.01 ;
     }
   _ltp = log ( taue / taum ) / ( ( 1.0 / taum ) - ( 1.0 / taue ) ) ;
   be = 1.0 / ( exp ( - _ltp / taum ) - exp ( - _ltp / taue ) ) ;
   ae = be * ( ( taum / taue ) - 1.0 ) ;
   _ltp = log ( taui / taum ) / ( ( 1.0 / taum ) - ( 1.0 / taui ) ) ;
   bi = 1.0 / ( exp ( - _ltp / taum ) - exp ( - _ltp / taui ) ) ;
   ai = bi * ( ( taum / taui ) - 1.0 ) ;
    return 0; }
 
static double _hoc_factors(void* _vptr) {
 double _r;
    _hoc_setdata(_vptr);
 _r = 1.;
 factors (  );
 return(_r);
}
 
double tf (  double _lbb ) {
   double _ltf;
 if ( _lbb > 1.0 ) {
     _ltf = taum * log ( ( _lbb - m ) / ( _lbb - 1.0 ) ) ;
     }
   else {
     _ltf = 1e9 ;
     }
   
return _ltf;
 }
 
static double _hoc_tf(void* _vptr) {
 double _r;
    _hoc_setdata(_vptr);
 _r =  tf (  *getarg(1) );
 return(_r);
}
 
double firetimebound (  ) {
   double _lfiretimebound;
 double _lh , _ltemp ;
 _lh = ae * e + ai * i ;
   if ( b > 1.0 ) {
     if ( _lh > 0.0 ) {
       _lfiretimebound = tf ( _threadargscomma_ _lh + b ) ;
       }
     else {
       _ltemp = tf ( _threadargscomma_ b ) ;
       _ltemp = tf ( _threadargscomma_ b + _lh * exp ( - _ltemp / taui ) ) ;
       _lfiretimebound = tf ( _threadargscomma_ b + _lh * exp ( - _ltemp / taui ) ) ;
       }
     }
   else {
     if ( _lh + b > 1.0 ) {
       _lfiretimebound = tf ( _threadargscomma_ _lh + b ) ;
       }
     else {
       _lfiretimebound = 1e9 ;
       }
     }
   
return _lfiretimebound;
 }
 
static double _hoc_firetimebound(void* _vptr) {
 double _r;
    _hoc_setdata(_vptr);
 _r =  firetimebound (  );
 return(_r);
}
 
static int  seed (  double _lx ) {
   set_seed ( _lx ) ;
    return 0; }
 
static double _hoc_seed(void* _vptr) {
 double _r;
    _hoc_setdata(_vptr);
 _r = 1.;
 seed (  *getarg(1) );
 return(_r);
}
 
static void _net_receive (_pnt, _args, _lflag) Point_process* _pnt; double* _args; double _lflag; 
{    _p = _pnt->_prop->param; _ppvar = _pnt->_prop->dparam;
  if (_tsav > t){ extern char* hoc_object_name(); hoc_execerror(hoc_object_name(_pnt->ob), ":Event arrived out of order. Must call ParallelContext.set_maxstep AFTER assigning minimum NetCon.delay");}
 _tsav = t;   if (_lflag == 1. ) {*(_tqitem) = 0;}
 {
   newstates ( _threadargscomma_ t - t0 ) ;
   update ( _threadargs_ ) ;
   if ( _lflag  == 1.0 ) {
     nself = nself + 1.0 ;
     if ( m > 1.0 - eps ) {
       if ( m > 1.0 + eps ) {
         printf ( "m>1 error in IF3 mechanism--m = %g\n" , m ) ;
         }
       net_event ( _pnt, t ) ;
       on = 0.0 ;
       m = 0.0 ;
       artcell_net_send ( _tqitem, _args, _pnt, t +  refrac , 2.0 ) ;
       _ztotal = _ztotal + 1.0 ;
       if ( _ztotal  == _znexttotal ) {
         _znexttotal = _znexttotal + 1000.0 ;
         }
       }
     else {
       artcell_net_send ( _tqitem, _args, _pnt, t +  firetimebound ( _threadargs_ ) , 1.0 ) ;
       }
     }
   else if ( _lflag  == 2.0 ) {
     on = 1.0 ;
     m = 0.0 ;
     artcell_net_send ( _tqitem, _args, _pnt, t +  firetimebound ( _threadargs_ ) , 1.0 ) ;
     }
   else {
     if ( s > 0.0 ) {
       k = normrand ( 1.0 , s ) ;
       }
     else {
       k = 1.0 ;
       }
     if ( k < 0.0 ) {
       k = 0.0 ;
       }
     if ( _args[0] * k > 0.0 ) {
       nexcite = nexcite + 1.0 ;
       e = e + _args[0] * k ;
       }
     else {
       ninhibit = ninhibit + 1.0 ;
       i = i + _args[0] * k ;
       }
     if ( on ) {
       artcell_net_move ( _tqitem, _pnt, firetimebound ( _threadargs_ ) + t ) ;
       }
     }
   } }

static void initmodel() {
  int _i; double _save;_ninits++;
{
 {
   factors ( _threadargs_ ) ;
   e = 0.0 ;
   i = 0.0 ;
   m = 0.0 ;
   t0 = t ;
   on = 1.0 ;
   artcell_net_send ( _tqitem, (double*)0, _ppvar[1]._pvoid, t +  firetimebound ( _threadargs_ ) , 1.0 ) ;
   nself = 0.0 ;
   nexcite = 0.0 ;
   ninhibit = 0.0 ;
   _ztotal = 0.0 ;
   _znexttotal = 1000.0 ;
   b = bTest + bOffset ;
   }

}
}

static void nrn_init(_NrnThread* _nt, _Memb_list* _ml, int _type){
Node *_nd; double _v; int* _ni; int _iml, _cntml;
#if CACHEVEC
    _ni = _ml->_nodeindices;
#endif
_cntml = _ml->_nodecount;
for (_iml = 0; _iml < _cntml; ++_iml) {
 _p = _ml->_data[_iml]; _ppvar = _ml->_pdata[_iml];
 _tsav = -1e20;
 initmodel();
}}

static double _nrn_current(double _v){double _current=0.;v=_v;{
} return _current;
}

static void nrn_state(_NrnThread* _nt, _Memb_list* _ml, int _type){
Node *_nd; double _v = 0.0; int* _ni; int _iml, _cntml;
#if CACHEVEC
    _ni = _ml->_nodeindices;
#endif
_cntml = _ml->_nodecount;
for (_iml = 0; _iml < _cntml; ++_iml) {
 _p = _ml->_data[_iml]; _ppvar = _ml->_pdata[_iml];
 _nd = _ml->_nodelist[_iml];
 v=_v;
{
}}

}

static void terminal(){}

static void _initlists() {
 int _i; static int _first = 1;
  if (!_first) return;
_first = 0;
}

#if NMODL_TEXT
static const char* nmodl_filename = "if3Rand2.mod";
static const char* nmodl_file_text = 
  "COMMENT\n"
  "Total current is b + weighted events that decay with time constants taue and taui.\n"
  "Total current is integrated, and when this passes 1, then fire.\n"
  "That is\n"
  "\n"
  "taue * de/dt + e = 0\n"
  "taui * di/dt + i = 0\n"
  "  where an event at time t1 causes e or i to be incremented by amount w, \n"
  "  depending on sign of w, i.e.\n"
  "  if (w > 0) then e += w\n"
  "  else i += w\n"
  "  and\n"
  "taum*dm/dt + m = ae*e + ai*i + b\n"
  "  where fire if m crosses 1 in a positive direction\n"
  "  and after firing m is reset to 0\n"
  "\n"
  "Requires that taue <= taui, but taum can be larger or smaller than either \n"
  "or both of the synaptic time constants.\n"
  "\n"
  "Extended to include synaptic noise - PJ, 16.12.2018 - s = standard deviation, k = scaling factor \n"
  "See Carnevale and Hines, 2008, in: Soltesz and Staley, Computational Neuroscience in Epilepsy, AP, p. 28\n"
  "https://www.neuron.yale.edu/ftp/ted/neuron/ \n"
  "Further extended to include cellular heterogeneity - PJ, 31.03.2021 - using bOffset  \n"
  "\n"
  "ENDCOMMENT\n"
  "\n"
  "NEURON {\n"
  "	ARTIFICIAL_CELL IF3Rand2 : PJ - rename\n"
  "	RANGE taue, taui, taum, e, i, m, b, s, k, bTest, bOffset : PJ\n"
  "	RANGE nself, nexcite, ninhibit\n"
  "	GLOBAL eps, refrac\n"
  "}\n"
  "\n"
  "PARAMETER {\n"
  "	: specify limits on these\n"
  "	taue = 3 (ms) < 1e-9, 1e9 >\n"
  "	taui = 10 (ms) < 1e-9, 1e9 >\n"
  "	taum = 30 (ms) < 1e-9, 1e9 >\n"
  "	eps = 1e-6\n"
  "	refrac = 5 (ms)\n"
  "	s = 0 (1) <0, 1> : standard deviation PJ\n"
  "	bTest = 0 : : driving current PJ \n"
  "	bOffset = 0 : offset for cellular heterogeneity PJ \n"
  "\n"
  "}\n"
  "\n"
  "ASSIGNED {\n"
  "	e i m\n"
  "	enew inew mnew\n"
  "	t0 (ms)\n"
  "	nself nexcite ninhibit\n"
  "\n"
  "	ae ai\n"
  "	be bi\n"
  "	on\n"
  "	b : PJ\n"
  "	k : multiplier of synaptic weight \n"
  "}\n"
  "\n"
  ": argument is the interval since last update\n"
  "PROCEDURE newstates(d(ms)) {\n"
  "	LOCAL ee, ei, em\n"
  "\n"
  "	ee = exp(-d/taue)\n"
  "	ei = exp(-d/taui)\n"
  "	em = exp(-d/taum)\n"
  "\n"
  "	enew = e*ee\n"
  "	inew = i*ei\n"
  "	mnew = b + (m - b)*em\n"
  "		+ ae*e*(taue/(taum - taue))*(em - ee)\n"
  "		+ ai*i*(taui/(taum - taui))*(em - ei)\n"
  "}\n"
  "\n"
  "FUNCTION M() {\n"
  "	if (on) {\n"
  "		newstates(t - t0)\n"
  "		M = mnew\n"
  "	}else{\n"
  "		M = 0\n"
  "	}\n"
  "}\n"
  "\n"
  "FUNCTION E() {\n"
  "	newstates(t - t0)\n"
  "	E = enew\n"
  "}\n"
  "\n"
  "FUNCTION I() {\n"
  "	newstates(t - t0)\n"
  "	I = inew\n"
  "}\n"
  "\n"
  "PROCEDURE update() {\n"
  "	e = enew\n"
  "	i = inew\n"
  "	m = mnew\n"
  "	t0 = t\n"
  "}\n"
  "\n"
  "PROCEDURE factors() {\n"
  "	LOCAL tp\n"
  "	: force all exponential solution ( no x*exp(-x) )\n"
  "	: and force assertion for correctness of algorithm\n"
  "	: i.e. taue is smallest and only one that is excitatory\n"
  "	if (taue >= taui) { taui = taue + 0.01 }\n"
  "	if (taum == taue) { taum = taue + 0.01 }\n"
  "	if (taum == taui) { taum = taui + 0.01 }\n"
  "\n"
  "	: normalize so the peak magnitude of m is 1 when single e of 1\n"
  "	tp = log(taue/taum)/((1/taum) - (1/taue))\n"
  "	be = 1/(exp(-tp/taum) - exp(-tp/taue))\n"
  "	ae = be*((taum/taue) - 1)\n"
  "\n"
  "	: normalize so the peak magnitude of m is -1 when single i of -1\n"
  "	tp = log(taui/taum)/((1/taum) - (1/taui))\n"
  "	bi = 1/(exp(-tp/taum) - exp(-tp/taui))\n"
  "	ai = bi*((taum/taui) - 1)\n"
  "}\n"
  "\n"
  "FUNCTION tf(bb) (ms) {\n"
  "	if (bb > 1) { tf = taum*log((bb-m)/(bb-1)) }\n"
  "	else { tf = 1e9 }\n"
  "}\n"
  "\n"
  "FUNCTION firetimebound() (ms) {\n"
  "	LOCAL h, temp\n"
  "	h = ae*e + ai*i\n"
  "	if (b>1) {\n"
  "		if (h>0) {\n"
  "			firetimebound = tf(h+b)\n"
  "		} else {\n"
  "			: take net inhibitory current into account, at least partially\n"
  "			temp = tf(b) : too early\n"
  "			temp = tf(b + h*exp(-temp/taui)) : too late\n"
  "			: later than too early, but not too late\n"
  "			firetimebound = tf(b + h*exp(-temp/taui))\n"
  "		}\n"
  "	} else {\n"
  "		: this compound conditional can be simplified\n"
  ":		if ((h+b-m > 0) && (h+b > 1)) {\n"
  "		: h+b-m > 0 implies h+b > m, but h+b > 1 guarantees h+b > m since 1 >= m\n"
  "		if (h+b > 1) {\n"
  "			firetimebound = tf(h+b)\n"
  "		} else {\n"
  "			firetimebound = 1e9\n"
  "		}\n"
  "	}\n"
  ":	printf(\"firetimebound=%g\\n\", firetimebound)\n"
  "}\n"
  "\n"
  "PROCEDURE seed(x) { : PJ\n"
  "         set_seed(x)\n"
  "}\n"
  "\n"
  "LOCAL total, nexttotal\n"
  "\n"
  "INITIAL {\n"
  "	factors()\n"
  "	e = 0\n"
  "	i = 0\n"
  "	m = 0\n"
  "	t0 = t\n"
  "	on = 1\n"
  "	net_send(firetimebound(), 1)\n"
  "\n"
  "	nself=0\n"
  "	nexcite=0\n"
  "	ninhibit=0\n"
  "	total = 0\n"
  "	nexttotal = 1000\n"
  "	b = bTest + bOffset : PJ\n"
  "}\n"
  "\n"
  "NET_RECEIVE (w) {\n"
  "	newstates(t-t0)\n"
  "	update()	\n"
  ":printf(\"event %g t=%g e=%g i=%g m=%g\\n\", flag, t, e, i, m)\n"
  "	if (flag == 1) { :determine whether to fire or try again\n"
  "		nself = nself + 1\n"
  "		if (m > 1-eps) { : time to fire\n"
  "			if (m > 1+eps) { : oops!\n"
  "				printf(\"m>1 error in IF3 mechanism--m = %g\\n\", m)\n"
  "				: how to quit?\n"
  "			}\n"
  "			:printf(\"fire %g\\n\", t)\n"
  "			net_event(t)\n"
  "			on = 0\n"
  "			m = 0\n"
  "			net_send(refrac, 2) : start refractory state\n"
  "			total = total + 1\n"
  "			if (total == nexttotal) {\n"
  ":				printf(\"IF3 total spikes %g at %g\\n\", total, t)\n"
  "				nexttotal = nexttotal + 1000\n"
  "			}\n"
  "		}else{ : try again\n"
  "			net_send(firetimebound(), 1)\n"
  "		} \n"
  "	}else if (flag == 2) { : end of refractory period\n"
  "		on = 1\n"
  "		m = 0\n"
  "		net_send(firetimebound(), 1)\n"
  "	} else {\n"
  "	    if (s>0) { k = normrand(1, s) } else { k = 1 } : PJ \n"
  "		if (k<0) { k = 0 } \n"
  "		if (w*k > 0) {\n"
  "			nexcite = nexcite + 1\n"
  "			e = e + w*k\n"
  "		} else {\n"
  "			ninhibit = ninhibit + 1\n"
  "			i = i + w*k\n"
  "		}\n"
  ":printf(\"w=%g e=%g i=%g\\n\", w, e, i)\n"
  "		if (on) {\n"
  "			net_move(firetimebound() + t)\n"
  "		}\n"
  "	}\n"
  "}\n"
  "\n"
  "\n"
  "\n"
  "\n"
  "\n"
  "\n"
  ;
#endif
