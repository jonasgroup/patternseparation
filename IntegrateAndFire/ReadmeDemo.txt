ReadmeDemo.doc
Neuron code for simulations of EC � DG � CA3 network. 
Original paper: 
Guzman SJ, Schl�gl A, Espinoza C, Zhang X Suter BA, Jonas P (2021)
How connectivity rules and synaptic properties shape the efficacy of pattern separation in the entorhinal cortex�dentate gyrus�CA3 network. 
Nature Computational Science, revised version (NATCOMPUTSCI-20-0395)

Copyright (C) 2021, Peter Jonas, IST Austria, Peter.Jonas@ist.ac.at 
This file is part of PatternSeparation.
PatternSeparation is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

1. System requirements 
Intel core i7, 64 GByte RAM minimum. 
Windows 64 or Linux operating system. 
Code was tested on Windows 10 and Debian GNU/Linux (ver 9), using Neuron 7.7 for Windows or Linux. 

2. Installation 
Install recent version of Neuron (https://www.neuron.yale.edu/neuron/download). 
Create directory for simulations (e.g. Test). 
Create subdirectory for simulation input and output (e.g. patterns).  
Unzip SupplementarySoftware.7z to get hoc, mod, and in files (7-Zip 18.06 was used for generation). 
Copy hoc file into the directory. In the demo, this is simulation_Demo.hoc.   
Copy necessary mod files into the directory. These include: exp2synRand.mod, gap.mod, if3.mod, if3Rand.mod, and net_hh_w.mod. 
Copy the drive patterns into the subdirectory (e.g. patterns). There are two types of files: binary patterns (binaryPattern_x_y_n.in, n = 1 � 100) and analog patterns (drive_n.in, n = 1 � 100).  
Run mknrndll to compile the mod files. This should result in an nrnmech.dll file with the Windows operating system. 
The installation time is ~1 hour. 

3. Demo 
Douple-click on the demo hoc file (simulation_Demo.hoc). Or run nrngui, load the dll, and load the demo hoc file (simulation_Demo.hoc). 
The computation time is approximately 19 hours for the standard network using the above mentioned hardware (~1 hour for network initialization, ~18 hours for simulation of 100 patterns).
The following text file output should be generated in the sim0 subdirectory: drive_n.in, pattern_n.out, patternCA3_n.out, voltage_n.out, and raster_n.out (n = 1 � 100).

drive_n.in represents the drive in 500000 granule cells (input pattern). 
pattern_n.out should provide activity in 500000 granule cells (output pattern, 0 = silent, 1 = active). 
patternCA3_n.out should provide activity in 200000 CA3 pyramidal cells (output pattern, 0 = silent, 1 = active).

Thus, input-output correlation plots (RR plots) can be directly computed from the drive and pattern text files (e.g. Guzman et al., Fig. 2c). 

voltage_n out should give voltage traces (see Fig. 2b, top).  
raster_n out should give rasterplots (see Fig. 2b, bottom). 

simulation_Demo.hoc file uses the analog patterns in GCs with driveFileFlag = 1 (~19 hours computation time). 
Alternatively, set driveFileFlag = 0 to use the binary patterns in EC neurons (~3 days computation time). 



