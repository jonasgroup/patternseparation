# PatternSeparation

This repository contains the code that was used in [[1]](Guzman et al 2021).

[1] Guzman SJ, Schlögl A, Espinoza C, Zhang X Suter BA, Jonas P (2021)
How connectivity rules and synaptic properties shape the efficacy of pattern separation in the entorhinal cortex<96>dentate gyrus<96>CA3 network.
Nature Computational Science, revised version (NATCOMPUTSCI-20-0395)

Copyright (C) 2021, Peter Jonas, IST Austria, Peter.Jonas@ist.ac.at
This file is part of PatternSeparation.
PatternSeparation is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.


## System requirements
Intel core i7, 64 GByte RAM minimum.
Windows 64 or Linux operating system.
Code was tested on Windows 10 and Debian GNU/Linux using Neuron 7.7 and 7.8 .


## Installation
Install recent version of Neuron (https://www.neuron.yale.edu/neuron/download).

```
   git clone https://git.ist.ac.at/jonasgroup/patternseparation
```


Create directory for simulations (e.g. Test). Create subdirectory for simulation input and output (e.g. patterns). Unzip [download.zip](https://git.ist.ac.at/jonasgroup/patternseparation/-/archive/main/patternseparation-main.zip) to get hoc, mod, and in files.

Copy hoc file from `IntegrateAndFire/*.hoc` into your working directory. In the demo, this is `simulation_Default.hoc`. Copy necessary mod files into the directory. These include: `exp2synRand.mod`, `gap.mod`, `if3.mod`, `if3Rand.mod`, and `net_hh_w.mod`, and run `mknrndll` (on Windows) or `nrnivmodl` (on GNU/Linux) to compile the mod files. This should result in an `nrnmech.dll` file with the Windows operating system (on GNU Linux it is `x86_64/libnrnmech.so` ).

Copy the drive patterns into the subdirectory, precomputed drive patterns are available in the directory `patterns/*in`. There are two types of files: binary patterns (`binaryPattern_x_y_n.in`, n = 1 <85> 100) and analog patterns (`drive_n.in`, n = 1 <85> 100).

## Demo

Double-click on the demo hoc file (simulation_Default.hoc). Or run nrngui, load the dll, and load the demo hoc file (simulation_Default.hoc).
The computation time is approximately 19 hours for the standard network using the above mentioned hardware (~1 hour for network initialization, ~18 hours for simulation of 100 patterns).
The following text file output should be generated in the specified output directory `pathOut`: `drive_n.in`, `pattern_n.out`, `patternCA3_n.out`, `voltage_n.out`, and `raster_n.out` (n = 1 <85> 100).

`drive_n.in` represents the drive in 500000 granule cells (input pattern).
`pattern_n.out` should provide activity in 500000 granule cells (output pattern, 0 = silent, 1 = active).
`patternCA3_n.out` should provide activity in 200000 CA3 pyramidal cells (output pattern, 0 = silent, 1 = active).

Thus, input-output correlation plots (RR plots) can be directly computed from the drive and pattern text files (e.g. Guzman et al., Fig. 2c).

voltage_n out should give voltage traces (see Fig. 2b, top).
raster_n out should give rasterplots (see Fig. 2b, bottom).

simulation_Default.hoc file uses the analog patterns in GCs with driveFileFlag = 1 (~19 hours computation time).
Alternatively, set driveFileFlag = 0 to use the binary patterns in EC neurons (~ 3 days computation time).


On Linux, quick start would be run the simulation requires the following steps.

```
    git clone https://git.ist.ac.at/jonasgroup/patternseparation
    cd IntegrateAndFire
    nrnivmodl
    nrngui simulation_Demo.hoc
```

More examples of non-default simulations are available in

```
	IntegrateAndFire/simulation_%%%.hoc
```

The variable `pathIn` in the `simulation_*.hoc` to point to the directory with the input patterns. The variable `pathWD` in the `simulation_*.hoc` point to the working directory and can be adapted to your needs.

In case you want to run the simulations on a cluster with slurm scheduling system.  There are some utility functions
```
    utilities/job.default.slurm
```

Postprocessing (i.e. computing the correlations, and PSI plots) can be computed and then stored as ascii, hdf5, and matlab files, using the Matlab/Octave script:

```
    utilities/do_correlation_analysis.m
```


