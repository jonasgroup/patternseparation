function do_correlation_analysis(TaskID, maxP)
% ANALYZE_CORRELATIONS of Input and Output patterns
%
%
% (C) 2017-2021 Alois Schloegl, IST Austria

function psi=get_psi(tmp)
        tmp = [0,0;1,1;tmp(~any(isnan(tmp),2),:)];
	r   = tmp(:,1) + i*(1-tmp(:,2));
	phi = angle(r);
	[tmptmp,ix] = sort(phi);
	psi(1,1) = 2*trapz(phi(ix)/2,abs(r(ix)).^2) - 1;

	r1   = (1-tmp(:,1)) + i*tmp(:,2);
	phi1 = angle(r1);
	[tmptmp,ix] = sort(phi1);
	psi(2,1) = 1-2*trapz(phi1(ix)/2,abs(r1(ix)).^2);
end   % function get_psi

if nargin<2, maxP=100; end 

ID0=tic; cput0=cputime();

	ID1=tic; cput1=cputime();
	fprintf(1,'timing: (%d)\t%g\t%g\n', 0, toc(ID1), cputime()-cput1);

	prefix='standardPatterns/';
	if ~isdir(prefix), prefix=''; end;
	for k = 1:maxP;
		x = load(sprintf('%sbinaryPattern_nEC=50000a=0.1_%d.in',prefix,k));
		if k==1, X=zeros(length(x),maxP); end;
		X(:,k) = x(:);
		[k,size(x),size(X)]
	end
	meanBIN=full(mean(X));
	[n,m] = size(X);
	CCbin = my_cc(X);
	fprintf(1,'timing: (%d)\t%g\t%g\n',0, toc(ID1), cputime()-cput1);


for taskId=reshape(TaskID,1,[]),

	tid=sprintf('sim%d',taskId);
	% tid='simDefault';
	
if ~exist(sprintf('%s/pattern_%d.out',tid,maxP),'file') continue; end

	ID1=tic; cput1=cputime();
	fprintf(1,'timing: (%d)\t%g\t%g\n',taskId, toc(ID1), cputime()-cput1);

	for k = 1:maxP;
		x = load(sprintf('%s/drive_%d.in',tid,k));
		if k==1, X=zeros(length(x),maxP); end;
		X(:,k) = x(:);
	end
	meanEC=full(mean(X));
	[n,m] = size(X);
	CCin = my_cc(X);
	fprintf(1,'timing: (%d)\t%g\t%g\n',taskId, toc(ID1), cputime()-cput1);

        X  = sparse(n,m);
        for k = 1:maxP;
	    ff = sprintf('%s/pattern_%d.out',tid,k);
	    %disp(ff)
            x = load(ff);
            if k==1, X=sparse(length(x),maxP); end;
            X(find(x~=0),k) = 1;
        end
	meanDG = full(mean(X));

	CCout=my_cc(X);

	fprintf(1,'timing: (%d)\t%g\t%g\n',taskId, toc(ID1), cputime()-cput1);

	CCoutCa3=[];
	meanCa3 =[];

ca3flag = exist(sprintf('%s/patternCA3_%d.out',tid,maxP),'file');

if ca3flag
        X  = sparse(n,m);
        for k = 1:maxP;
	    ff = sprintf('%s/patternCA3_%d.out',tid,k);
	    %disp(ff)
            x = load(ff);
            if k==1, X=sparse(length(x),maxP); end;
            X(find(x~=0),k) = 1;
        end
	meanCa3= full(mean(X));

	CCoutCa3 = my_cc(X);

	fprintf(1,'timing: (%d)\t%g\t%g\n',taskId, toc(ID1), cputime()-cput1);

        CCoutCa3(1:m+1:end)=NaN;
else
	CCoutCa3=repmat(NaN,100,100);
	meanCa3=repmat(NaN,1,maxP);
end

	[ix,iy] = meshgrid(1:maxP);
	fid1=fopen(sprintf('CC_results.in.%s.txt',tid),'w');
	if ca3flag
		fprintf(fid1,'%d\t%d\t%.8f\t%.8f\t%.8f\n',[ix(ix>iy),iy(ix>iy),CCin(ix>iy), CCout(ix>iy), CCoutCa3(ix>iy)]');
	else
		fprintf(fid1,'%d\t%d\t%.8f\t%.8f\n',[ix(ix>iy),iy(ix>iy),CCin(ix>iy), CCout(ix>iy)]');
	end
	fclose(fid1);
	fid1=fopen(sprintf('CC_results.bin.%s.txt',tid),'w');
	if ca3flag
		fprintf(fid1,'%d\t%d\t%.8f\t%.8f\t%.8f\n',[ix(ix>iy),iy(ix>iy),CCbin(ix>iy), CCout(ix>iy), CCoutCa3(ix>iy)]');
	else
		fprintf(fid1,'%d\t%d\t%.8f\t%.8f\n',[ix(ix>iy),iy(ix>iy),CCbin(ix>iy), CCout(ix>iy)]');
	end
	fclose(fid1);

	% compute Area using trapez rule in polar coordinates 
	% r*exp(-i*phi) = CCin+i*CCout; 
	% A = integral{r(phi).^2,dphi}/2 
	% Psi is area between the identiy line any the curve multiplied by 2.  
	psi = repmat(NaN,2,5);

	% EC-CA3
	if ca3flag
		psi(:,1) = get_psi([CCin(:),CCoutCa3(:)]);
		psi(:,4) = get_psi([CCbin(:),CCoutCa3(:)]);
	end

	% EC-DG
	psi(:,2) = get_psi([CCin(:),CCout(:)]);
	psi(:,5) = get_psi([CCbin(:),CCout(:)]);

	if ca3flag
		% DG-CA3
		psi(:,3) = get_psi([CCout(:),CCoutCa3(:)]);
	end

	activity=[meanEC;meanDG;meanCa3;meanBIN];
        fid=fopen(sprintf('act_results.%s.txt',tid),'w');
	if ca3flag
		% fprintf(fid,'#\tEC\tDG\tCa3\r\n');
		fprintf(fid,'%d\t%g\t%g\t%g\r\n',[[1:100];meanEC;meanDG;meanCa3]);
	else
		% fprintf(fid,'#\tEC\tDG\r\n');
		fprintf(fid,'%d\t%g\t%g\r\n',[[1:100];meanEC;meanDG]);
	end
	fclose(fid);

	% save('-mat',sprintf('CC_results.%s.mat',tid), 'CCin','CCin3','CCout','CCout3','CCoutCa3','CCout3Ca3','n','m','psi');
	save(sprintf('CC_results.%s.mat',tid), '-mat','CCin','CCout','CCoutCa3','CCbin','n','m','psi','activity');
	save(sprintf('CC_results.%s.h5',tid), '-hdf5','CCin','CCout','CCoutCa3','CCbin','n','m','psi','meanEC','meanDG','meanCa3','meanBIN','activity');

	tmp = [CCin(:),CCout(:),CCoutCa3(:),CCbin(:)];

	fid1=fopen(sprintf('psi.%s.txt',tid),'w');
	fprintf(fid1,'#\tEC-CA3\tEC-DG\tDG-CA3\tBIN-CA3\tBIN-DG\n');
	fprintf(fid1,'%.6f\t%.6f\t%f\t%f\t%f\n',psi');
	fclose(fid1);


	clf
        subplot(3,2,1)
        plot([meanDG;meanCa3;meanBIN]');
	ylabel('activity');
	legend({'DG','Ca3','BIN'});
        TIT = tid; 
        title(TIT);

        subplot(3,2,2);
        h = plot(CCin,CCout,'xk');
	axis square
        set(gca,'xlim',[0,1],'ylim',[0,1]);
        xlabel('cc_{EC}');
        ylabel('cc_{DG}');
        text(.1,.8,sprintf('psi=%f (%f)',psi(:,2)))

        subplot(3,2,5);
        h = plot(CCbin,CCout,'xk');
	axis square
        set(gca,'xlim',[0,1],'ylim',[0,1]);
        xlabel('cc_{bin}');
        ylabel('cc_{DG}');
        text(.1,.8,sprintf('psi=%f (%f)',psi(:,5)))

if ca3flag
        subplot(3,2,3);
        h = plot(CCin,CCoutCa3,'xk');
	axis square
        set(gca,'xlim',[0,1],'ylim',[0,1]);
        xlabel('cc_{EC}');
        ylabel('cc_{Ca3}');
        text(.1,.8,sprintf('psi=%f (%f)',psi(:,1)))

        subplot(3,2,6);
        h = plot(CCbin,CCoutCa3,'xk');
	axis square
        set(gca,'xlim',[0,1],'ylim',[0,1]);
        xlabel('cc_{BIN}');
        ylabel('cc_{Ca3}');
        text(.1,.8,sprintf('psi=%f (%f)',psi(:,4)))

        subplot(3,2,4);
        h = plot(CCout,CCoutCa3,'xk');
	axis square
        set(gca,'xlim',[0,1],'ylim',[0,1]);
        xlabel('cc_{DG}');
        ylabel('cc_{Ca3}');
        text(.1,.8,sprintf('psi=%f (%f)',psi(:,3)))
end
        drawnow
        print('-dpng',sprintf('CC_results.%s.png',tid))

continue

	tmpfile=sprintf('%s/CC_results.%s.txt',p,tid);
        fid = fopen(tmpfile,'w');
	if (fid<0)	
		error(sprintf('Can not open file %s',tmpfile));
	end
        fprintf(fid,'%f\t%f\n',tmp');
        fclose(fid);

	tmpfile=sprintf('%s/CC_results.%s.txt',pout,tid);
        fid = fopen(tmpfile,'w');
	if (fid<0)	
		error(sprintf('Can not open file %s',tmpfile));
	end
        fprintf(fid,'%f\t%f\n',tmp');
        fclose(fid);

end %% end for 

% continue 
% return

subplot(2,2,2)
plot(CCin,CCout0,'x')
xlabel('cc_{in}');
ylabel('cc_{out}');

subplot(2,2,3)
plot(CCout,CCout0,'x')
xlabel('cc_{out}');
ylabel('cc_{out0}');

end	% end function do_correlation_analysis

