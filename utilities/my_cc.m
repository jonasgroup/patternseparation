function [CC] = my_cc(X)
% MY_CC computes the correlation matrix
% is a helper function for do_correlation_analysis
%
% (C) 2017-2021 Alois Schloegl, IST Austria


	
[n,m]=size(X);
if 0, all(all(X==1 | X==0))
	[ix,iy]=meshgrid(1:m);
	CC = repmat(NaN,m,m);
	for k=1:numel(ix)
		
		if ix(k)==iy(k), disp(ix(k)); continue; end 
		% HIS = histo4(X(:,[ix,iy]));
		H   = sparse(X(:,ix(k))+1,X(:,iy(k))+1,1,2,2);
		CC(ix(k),iy(k)) = det(H) / sqrt(prod([sum(H), sum(H')]));
	end

else
	CC    = full(X'*X);
	S     = sum(X);
	M1    = S/n;
	% cc    = CC/n - n*M1'*M1;
	NNCC   = n*CC - S'*S;
	nn1v     = diag(NNCC);
	% v(v<0)=0; v=v/((n-1)*n);
	CC    = (n-1)*NNCC./(n*sqrt(nn1v*nn1v'));
	CC(1:m+1:end)=NaN;

end